# Functional Dependency Tool
A tool for solving functional dependency from existing data.

## Usage
```sh
fd [-i Input] [-o Output]
```
Defaults to standard input and output when no parameters are given.

## License
[MIT License](LICENSE)
