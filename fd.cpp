#include <string>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <utility>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

//Hash types
template <typename T>
struct VectorHasher;
template <typename T, typename U>
struct PairHasher;
//Attribute and attributes type
typedef size_t Attr;
typedef vector<Attr> Attrs;
//Value, record and table type
typedef string Value;
typedef vector<Value> Record;
typedef vector<Record> Table;
//Attributes map type
template <typename T>
using AttrsMap = unordered_map<Attrs, T, VectorHasher<Attr>>;
//Distinct value map type
typedef unordered_map<
    pair<size_t, Value>,
    size_t,
    PairHasher<size_t, Value>
> DistinctValueMap;
//Functional dependency
typedef pair<Attrs, Attr> Fd;
typedef vector<Fd> Fds;

//Partition type
struct Part {
    //Number of distinct values
    size_t n_distinct_values;
    //Occurance of values
    vector<pair<size_t, size_t>> occurance;
};

//Vector hasher
template <typename T>
struct VectorHasher {
    size_t operator()(const vector<T>& vec) const {
        hash<T> hasher;
        size_t hash = 0;

        for (size_t i=0;i<vec.size();i++) {
            size_t shift_bits = (i*4)%sizeof(size_t);
            size_t part_hash = hasher(vec[i]);

            part_hash = (part_hash<<shift_bits)+(part_hash>>(sizeof(size_t)-shift_bits));
            hash ^= part_hash;
        }

        return hash;
    }
};

//Pair hasher
template <typename T, typename U>
struct PairHasher {
    size_t operator()(const pair<T, U>& p) const {
        return hash<T>()(p.first)^hash<U>()(p.second);
    }
};

//Split a string by given character
vector<string> split(string& str, char delimiter) {
    vector<string> result;
    string temp;

    for (char c : str) {
        if (c==delimiter) {
            result.push_back(temp);
            temp.clear();
        } else {
            temp += c;
        }
    }
    result.push_back(temp);

    return result;
}

//Load table from stream
Table load_table(istream& in) {
    Table result;

    while (!in.eof()) {
        string line;

        getline(in, line);
        if (line.empty())
            continue;

        result.push_back(split(line, ','));
    }

    return result;
}

//Save functional dependecies to output stream
void save_fd(Fds& fds, ostream& out) {
    for (Fd& fd : fds) {
        for (Attr attr : fd.first)
            out<<attr<<' ';
        out<<"-> "<<fd.second<<endl;
    }
}

//Get sequence of attributes
vector<Attrs> attrs_seq(Attr begin, Attr end, size_t n_attrs) {
    vector<Attrs> result;

    //End of recursion
    if (n_attrs==1) {
        for (Attr i=begin;i<=end;i++)
            result.push_back({i});
        return result;
    }

    for (Attr i=begin;i<=end-n_attrs+1;i++) {
        vector<Attrs> recurse_result = attrs_seq(i+1, end, n_attrs-1);
        for (Attrs temp_attrs : recurse_result) {
            temp_attrs.insert(temp_attrs.begin(), i);
            result.push_back(temp_attrs);
        }
    }

    return result;
}

//Get attributes without a specific attribute
Attrs attrs_without(Attrs attrs, Attr without_attr) {
    Attrs result;

    for (Attr attr : attrs)
        if (attr!=without_attr)
            result.push_back(attr);

    return result;
}

//Get intersections of attributes
Attrs intersect(Attrs& a, Attrs& b) {
    Attrs result;

    if ((a.size()>0)&&(b.size()>0))
        for (Attr x : a)
            for (Attr y : b)
                if (x==y) {
                    result.push_back(x);
                    break;
                }

    return result;
}

//Solve partition
Part solve_part(Table& table, AttrsMap<Part>& part_map, Attrs attrs) {
    //Distinct values
    DistinctValueMap distinct_values;
    //Subset
    Attrs subset = attrs;
    //Last attribute
    Attr attr = attrs.back();
    //Result
    Part result;

    subset.pop_back();
    //Subset partition
    Part subset_part = part_map.at(subset);

    size_t i = 0;
    for (auto& value_occur : subset_part.occurance) {
        size_t j = 0;

        while (j<value_occur.second) {
            size_t same_count = 0;

            //Insert distinct value if it does not exist
            auto tuple = make_pair(value_occur.first, table[i][attr-1]);
            auto values_iter = distinct_values.find(tuple);
            size_t tuple_id = distinct_values.size();

            if (values_iter==distinct_values.end())
                distinct_values[tuple] = tuple_id;
            else
                tuple_id = values_iter->second;

            //Get count of consecutive tuples
            while ((i<table.size())&&(table[i][attr-1]==tuple.second)&&(j<value_occur.second)) {
                same_count++;
                i++;
                j++;
            }
            //Record tuple occurance
            result.occurance.push_back(make_pair(tuple_id, same_count));
        }
    }
    result.n_distinct_values = distinct_values.size();

    //Add partition result to map
    part_map[attrs] = result;

    return result;
}

//Solve functional dependencies
Fds solve_fd(Table& table) {
    //Number of attributes
    size_t n_attrs = table[0].size();
    //Partitions for no attributes
    Part empty_set_part;

    //Partitions
    AttrsMap<Part> part_map;
    //Right-hand side sets map
    AttrsMap<Attrs> rhs_map;
    //Result
    Fds fds;

    empty_set_part.n_distinct_values = 1;
    empty_set_part.occurance.push_back(make_pair(0, table.size()));
    part_map[{}] = empty_set_part;
    //Partitions for single attributes
    for (Attr i=1;i<=n_attrs;i++)
        solve_part(table, part_map, {i});
    //RHS for single attribute
    Attrs all_attrs;
    for (Attr i=1;i<=n_attrs;i++)
        all_attrs.push_back(i);
    for (Attr i=1;i<=n_attrs;i++)
        rhs_map[{i}] = all_attrs;

    //Level-wise computing
    for (size_t i=2;i<=n_attrs;i++) {
        for (Attrs& attrs : attrs_seq(1, n_attrs, i)) {
            //Solve attributes partition
            Part attrs_part = solve_part(table, part_map, attrs);

            //Calculate RHS
            Attrs rhs = all_attrs;
            for (Attr attr : attrs) {
                Attrs subset = attrs_without(attrs, attr);
                auto rhs_iter = rhs_map.find(subset);

                //Empty subset RHS
                if (rhs_iter==rhs_map.end()) {
                    rhs.clear();
                    break;
                } else {
                    rhs = intersect(rhs, rhs_iter->second);
                }
            }

            if (rhs.empty())
                continue;

            Attrs search_set = intersect(attrs, rhs);
            //Find functional dependecies
            for (Attr attr : search_set) {
                Attrs subset = attrs_without(attrs, attr);
                //Partition of subset
                Part subset_part = part_map.at(subset);

                //Found functional dependency
                if (attrs_part.n_distinct_values==subset_part.n_distinct_values) {
                    fds.push_back(make_pair(subset, attr));

                    //Remove attribute from RHS
                    auto attr_iter = find(rhs.begin(), rhs.end(), attr);
                    if (attr_iter!=rhs.end())
                        rhs.erase(attr_iter);
                }
            }

            //Save RHS set
            rhs_map[attrs] = rhs;
        }
    }

    //Sort functional dependencies
    sort(fds.begin(), fds.end(), [](Fd& x, Fd& y) {
        size_t x_lhs_size = x.first.size();
        size_t y_lhs_size = y.first.size();

        //Compare left-hand side data first
        for (unsigned int i=0;i<min(x_lhs_size, y_lhs_size);i++) {
            Attr x_val = x.first[i];
            Attr y_val = y.first[i];

            if (x_val==y_val)
                continue;
            else
                return x_val<y_val;
        }
        //Then left-hand side length
        if (x_lhs_size!=y_lhs_size)
            return x_lhs_size<y_lhs_size;

        //Finally right-hand side
        return x.second<y.second;
    });

    return fds;
}

//Program entry
int main(int argc, char** argv) {
    istream* in = &cin;
    ostream* out = &cout;

    //Parse command line arguments
    for (int i=1;i<argc;i++) {
        string arg_item = argv[i];

        //Input file
        if ((arg_item=="-i")&&(i<argc-1)) {
            i++;
            in = new ifstream(argv[i]);

            if (!in) {
                cerr<<"[Error] File input stream error"<<endl;
                return 1;
            }
        }
        //Output file
        else if ((arg_item=="-o")&&(i<argc-1)) {
            i++;
            out = new ofstream(argv[i]);

            if (!out) {
                cerr<<"[Error] File output stream error"<<endl;
                return 1;
            }
        }
        //Help
        else {
            cerr<<"Usage: "<<argv[0]<<" [-i Input] [-o Output]"<<endl;
            return 1;
        }
    }

    //Load dat
    Table table = load_table(*in);
    //Close and release input stream
    if (in!=&cin) {
        ifstream* fin = (ifstream*)in;

        fin->close();
        delete fin;
    }

    //Solve and save functional dependency
    Fds fds = solve_fd(table);
    save_fd(fds, *out);
    //Close and release output stream
    if (out!=&cout) {
        ofstream* fout = (ofstream*)out;

        fout->close();
        delete fout;
    }

    return 0;
}
