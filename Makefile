CXXFLAGS = -std=c++11 -O3
STRIP = strip

# Stripped release version
fd-release: fd
	$(STRIP) fd
# Clean
clean:
	rm -f fd
